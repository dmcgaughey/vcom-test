import _camelCase from 'lodash/camelCase'
import _mapKeys from 'lodash/mapKeys'
import _snakeCase from 'lodash/snakeCase'
import deepMapKeys from 'deep-map-keys'
import queryString from 'query-string'

const firstCharDigit = /^[0-9]/
const toCamelCase = (key) => key.match(firstCharDigit) ? key : _camelCase(key)
const toSnakeCase = (key) => key.match(firstCharDigit) ? key : _snakeCase(key)
const deepCamelCaseify = (obj) => deepMapKeys(obj, toCamelCase)
const deepSnakeCaseify = (obj) => deepMapKeys(obj, toSnakeCase)

export class ApiResult {
  constructor({ success, status, data }) {
    this.success = success
    this.status = status
    this.data = data ? deepCamelCaseify(data) : data
  }

  get isClientError() {
      return this.status && Math.floor(this.status/100) === 4
  }

  get isServerError() {
      return this.status && Math.floor(this.status/100) === 5
  }
}


export default class {
    constructor({ handlers = {} } = {}) {
      this._baseUrl = 'http://localhost:8888'
      //this._apiVersion = 2
      this.handlers = handlers
    }
  
    async _send({ path, method, query, body }) {
      let url = this._baseUrl + path
      let opts = {
        method,
        credentials: 'include',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          //'X-API-VERSION': this._apiVersion,
        }
      }
      if (query) {
        query = _mapKeys(query, (v, k) => toSnakeCase(k))
        url += '?' + queryString.stringify(query)
      }
      if (body) {
        opts.headers['Content-Type'] = 'application/json'
        opts.body = JSON.stringify(deepSnakeCaseify(body))
      }
  
      try {
        const res = await fetch(url, opts)
        if (res.status === 401 || res.status === 403) {
          if (this.handlers.unauthorized) {
            return this.handlers.unauthorized(res)
          } else {
            return new ApiResult({ success: false, status: res.status })
          }
        } else if (res.status === 500) {
          if (this.handlers.serverError) {
            return this.handlers.serverError(res)
          } else {
            return new ApiResult({ success: false, status: res.status })
          }
        }
        const json = await res.json()
        return new ApiResult({
          status: res.status,
          data: json
        })
      } catch (e) {
        console.error(e)
        if (this.handlers.fetchError) {
          return this.handlers.fetchError(e)
        } else {
            return new ApiResult({ success: false })
        }
      }
    }
  
    async _get(path, query) {
        return this._send({ method: 'GET', path, query })
    }
  
    async _post(path, body) {
        return this._send({ method: 'POST', path, body })
    }
  
    async _delete(path, body) {
        return this._send({ method: 'DELETE', path, body })
    }
  }