import VcomClient from './vcomtest_client'

class HotelClient extends VcomClient {
  constructor(...args) {
    super(...args)
  }

  //this route is not used right now but it would be my suggestion for
  // the route in order to make it dynamic for any hotel.
  async getHotelDetails(hotelCode) {
    return this._get('/api/hotels/' + hotelCode)
  }

  async getHotels() {
      return this._get('/api/hotels')
  }

  async getVenetianDetails() {
    return this._get('/api/hotels/venetian')
  }
}

const client = new HotelClient()
export default client

export const APIPlugin = {
  install(Vue) {
    Vue.prototype.$api = client
  }
}