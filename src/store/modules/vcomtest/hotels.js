import client from '../../../api_client'
import Vue from 'vue'

const state = {
  hotels: [],
  hotelLookup: {}
}

const getters = {
  hotels: (state) => state.hotels,
  hotelLookup: (state) => state.hotelLookup
}

const actions = {
  async fetchHotels({ commit }) {
    const res = await client.getHotels()
    commit('setHotels', res.data.list)
  },

  async fetchHotelDetails( { commit}, hotelCode) {
    //this wouldn't work right now but it is my recommendation for how to do a common route for
    // any hotel, assuming the hotel code is unique
    const res = await client.getHotelDetails(hotelCode)
    commit('updateHotelDetails', res.data)
  },


  // Without editing the hotel listing data, I have a hard coded api route for just the venentian.
  // Ideally, the venetian would be included in the api call for hotel listing, since 
  // it has a hotel code of 118.
  async fetchVenetianDetails( { commit }) {
    const res = await client.getVenetianDetails()
    commit('updateHotelDetails', res.data)
  }
}

const mutations = {
  setHotels(state, hotels) {
    var uniqueHotels = []
    for(var hotel of hotels) {
      if (!state.hotelLookup[hotel.code]){
        Vue.set(state.hotelLookup, hotel.code, hotel)
    
        state.hotelLookup[hotel.code] = hotel
        uniqueHotels.push(hotel)
      } 
    }

    uniqueHotels.sort(
      function(a, b){
        if(!a.name || !b.name){
          return 0;
        }
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        if (x < y) {return -1;}
        if (x > y) {return 1;}
        return 0;
      })
    state.hotels = uniqueHotels
  },

  updateHotelDetails(state, hotelDetails) {
    Vue.set(state.hotelLookup, hotelDetails.code, hotelDetails)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
