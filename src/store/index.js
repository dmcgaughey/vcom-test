import Vue from 'vue'
import Vuex from 'vuex'
import vcomtest from './modules/vcomtest'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    vcomtest
  }
})