import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import store from './store'

import client from './vcomtest_client'
import ApiResult from './vcomtest_client'

Vue.config.productionTip = false


client.handlers = {
  serverError(res) {
    console.error('Server Error', res)
    return new ApiResult({ success: false, status: res.status })
  },


  fetchError(res) {
    console.error('Fetch Error', res)
    return new ApiResult({ success: false })
  },
}

Vue.use(Vuetify)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
